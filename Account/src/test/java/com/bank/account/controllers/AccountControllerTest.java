package com.bank.account.controllers;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.assertj.core.util.DateUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.bank.account.models.DataStore;
import com.bank.account.service.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AccountControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private ObjectMapper mapper;
	
	@SpyBean
    private DataStore dataStore;
	
	@SpyBean
    private AccountService accountService;

	@Test()
	public void getBalance() {
		Mockito.when(dataStore.getBalance()).thenReturn(20.0);
		Double balance = this.restTemplate.getForObject("/account/balance", Double.class);
		assertEquals(balance, new Double(20));
	}

	@Test
	public void depositAmount() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0).thenCallRealMethod();
		Mockito.when(dataStore.getDepositLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 10000);
		Double balance = this.restTemplate.postForObject("/account/deposit", obj, Double.class);
		assertEquals(balance, new Double(10000));
	}
	
	@Test
	public void withdrawAmount() {
		double initialBalance = 30000.0;
		Mockito.when(dataStore.getBalance()).thenReturn(initialBalance).thenReturn(initialBalance).thenCallRealMethod();
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 10000);
		Double balance = this.restTemplate.postForObject("/account/withdraw", obj, Double.class);
		assertEquals(balance, new Double(20000));
	}
	
	//Insufficient funds
	@Test
	public void insufficientFundsWithdrawal() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0).thenCallRealMethod();
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 20000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, List.class);
		
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "withdrawalRequest: Insufficient account balance");
	}
	
	//Max number of deposits in a day (4)
	@Test
	public void maxDepositsInADay() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0);
		Mockito.when(dataStore.getDepositLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 20000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, List.class);
		
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "depositRequest: Cannot deposit amount. Maximum of only 4 deposits are allowed in a day");
	}
	
	//Max number of withdrawals in a day (3)
	@Test
	public void maxWithdrawalsInADay() {
		Mockito.when(dataStore.getBalance()).thenReturn(50000.0);
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 10000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, List.class);

		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "withdrawalRequest: Cannot withdraw amount. Maximum of only 3 withdrawls are allowed in a day");
	}
	
	//Max deposit amount per transaction (40k)
	@Test
	public void maxDepositAmount() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0);
		Mockito.when(dataStore.getDepositLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 50000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, List.class);

		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "amount: must be less than or equal to 40000");
	}
	
	//Max withdrawal amount per transaction (20k)
	@Test
	public void maxWithdrawAmount() {
		Mockito.when(dataStore.getBalance()).thenReturn(50000.0);
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 30000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, List.class);

		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "amount: must be less than or equal to 20000");
	}
	
	//Max deposit amount in a day (150k)
	@Test
	public void maxDepositAmountInADay() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0);
		Mockito.when(dataStore.getDepositLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 40000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, List.class);

		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "depositRequest: Cannot deposit amount. Maximum amount allowed to be deposited per day ($150k) exceeded");
	}

	//Max withdrawal amount in a day
	@Test
	public void maxWithdrawalAmountInADay() {
		Mockito.when(dataStore.getBalance()).thenReturn(150000.0).thenReturn(150000.0).thenCallRealMethod();
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 20000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		ResponseEntity<List> response = this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, List.class);

		System.out.println(response.getBody());
		assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
		assertEquals(response.getBody().get(0), "withdrawalRequest: Cannot withdraw amount. Maximum amount allowed to be withdrawn per day ($50k) exceeded");
	}

	//Deposit amount for next day
	@Test
	public void depositAmountForNextDay() {
		Mockito.when(dataStore.getBalance()).thenReturn(0.0).thenCallRealMethod();
		Mockito.when(dataStore.getDepositLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 40000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		//For Today
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);
		//For another day
		Mockito.when(accountService.getTodaysDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").format(DateUtil.yesterday()));
		ResponseEntity<Double> response = this.restTemplate.exchange("/account/deposit", HttpMethod.POST, req, Double.class);

		assertEquals(response.getBody(), new Double(160000));
	}
	
	//Withdrawal amount for next day
	@Test
	public void withdrawAmountForNextDay() {
		Mockito.when(dataStore.getBalance()).thenReturn(150000.0).thenReturn(150000.0).thenCallRealMethod();
		Mockito.when(dataStore.getWithdrawalLog()).thenReturn(new HashMap<>());
		
		ObjectNode obj = mapper.createObjectNode();
		obj.put("amount", 20000);
		
		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<ObjectNode> req = new HttpEntity<>(obj, headers);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);
		//For another day
		Mockito.when(accountService.getTodaysDate()).thenReturn(new SimpleDateFormat("yyyy-MM-dd").format(DateUtil.yesterday()));
		ResponseEntity<Double> response = this.restTemplate.exchange("/account/withdraw", HttpMethod.POST, req, Double.class);

		assertEquals(response.getBody(), new Double(90000));
	}
	
}
