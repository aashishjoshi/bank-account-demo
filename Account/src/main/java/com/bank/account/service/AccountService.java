package com.bank.account.service;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bank.account.models.DataStore;
import com.bank.account.models.DepositRequest;
import com.bank.account.models.WithdrawalRequest;

@Component
public class AccountService {

	private final DataStore dataStore;

	@Autowired
	public AccountService(DataStore dataStore) {
		this.dataStore = dataStore;
	}

	public double getBalance() {
		return dataStore.getBalance();
	}

	public List<Double> getTodaysDepositLog() {
		return dataStore.getDepositLog().get(getTodaysDate());
	}

	public List<Double> getTodaysWithdrawalLog() {
		return dataStore.getWithdrawalLog().get(getTodaysDate());
	}

	public Double depositAmount(DepositRequest deposit) {
		List<Double> log = dataStore.getDepositLog().get(getTodaysDate());
		if (log == null) {
			log = new ArrayList<>();
		}
		log.add(deposit.getAmount());
		dataStore.getDepositLog().put(getTodaysDate(), log);
		dataStore.setBalance(dataStore.getBalance() + deposit.getAmount());
		return dataStore.getBalance();
	}

	public Double withdrawAmount(WithdrawalRequest withdrawal) {
		List<Double> log = dataStore.getWithdrawalLog().get(getTodaysDate());
		if (log == null) {
			log = new ArrayList<>();
		}
		log.add(withdrawal.getAmount());
		dataStore.getWithdrawalLog().put(getTodaysDate(), log);
		dataStore.setBalance(dataStore.getBalance() - withdrawal.getAmount());
		return dataStore.getBalance();
	}

	public String getTodaysDate() {
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(new Date());
	}
}
