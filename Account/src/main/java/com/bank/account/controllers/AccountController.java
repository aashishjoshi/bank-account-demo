package com.bank.account.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bank.account.models.DepositRequest;
import com.bank.account.models.WithdrawalRequest;
import com.bank.account.service.AccountService;
import com.bank.account.validators.DepositValidator;
import com.bank.account.validators.WithdrawalValidator;

@RestController
@RequestMapping("/account")
public class AccountController {

	private final AccountService accountService;

	private final DepositValidator depositValidator;

	private final WithdrawalValidator withdrawalValidator;

	@Autowired
	public AccountController(DepositValidator depositValidator, WithdrawalValidator withdrawalValidator,
			AccountService accountService) {
		this.depositValidator = depositValidator;
		this.withdrawalValidator = withdrawalValidator;
		this.accountService = accountService;
	}

	@InitBinder("depositRequest")
	public void setupDepositBinder(WebDataBinder binder) {
		binder.addValidators(depositValidator);
	}

	@InitBinder("withdrawalRequest")
	public void setupWithdrawalBinder(WebDataBinder binder) {
		binder.addValidators(withdrawalValidator);
	}

	@RequestMapping("/balance")
	@ResponseBody
	Double balance() {
		return accountService.getBalance();
	}

	@RequestMapping(value = "/deposit", method = RequestMethod.POST)
	@ResponseBody
	Double deposit(@Valid @RequestBody DepositRequest req) {
		return accountService.depositAmount(req);
	}

	@RequestMapping(value = "/withdraw", method = RequestMethod.POST)
	@ResponseBody
	Double withdrawal(@Valid @RequestBody WithdrawalRequest req) {
		return accountService.withdrawAmount(req);
	}
}
