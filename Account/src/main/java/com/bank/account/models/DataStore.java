package com.bank.account.models;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.stereotype.Component;

@Component
public class DataStore {

	private AtomicReference<Double> balance = new AtomicReference<Double>(0.0);

	private ConcurrentMap<String, List<Double>> depositLog = new ConcurrentHashMap<String, List<Double>>();

	private ConcurrentMap<String, List<Double>> withdrawalLog = new ConcurrentHashMap<String, List<Double>>();

	public double getBalance() {
		return balance.get();
	}

	public void setBalance(double balance) {
		this.balance.set(balance);
		;
	}

	public Map<String, List<Double>> getDepositLog() {
		return depositLog;
	}

	public Map<String, List<Double>> getWithdrawalLog() {
		return withdrawalLog;
	}
}
