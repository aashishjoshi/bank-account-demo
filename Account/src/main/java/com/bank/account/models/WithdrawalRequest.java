package com.bank.account.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class WithdrawalRequest {

	@Min(value = 1)
	@Max(value = 20000)
	private double amount;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}
}
