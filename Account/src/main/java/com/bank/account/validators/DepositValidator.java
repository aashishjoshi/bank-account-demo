package com.bank.account.validators;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.bank.account.models.DepositRequest;
import com.bank.account.service.AccountService;

@Component
public class DepositValidator implements Validator {

	private final AccountService accountService;

	@Autowired
	public DepositValidator(AccountService accountService) {
		super();
		this.accountService = accountService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return DepositRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		DepositRequest deposit = (DepositRequest) obj;
		// Check for max transactions in the day
		List<Double> depositLogList = accountService.getTodaysDepositLog();
		if (depositLogList != null) {
			if (depositLogList.size() >= 4) {
				// add error for max number of transaction per day exceeded
				errors.reject(HttpStatus.BAD_REQUEST.toString(),
						"Cannot deposit amount. Maximum of only 4 deposits are allowed in a day");
			} else {
				Double totalDeposit = depositLogList.stream().mapToDouble(d -> d.doubleValue()).sum();
				// add error for max deposit amount for the day exceeded
				if (totalDeposit + deposit.getAmount() > 150000) {
					errors.reject(HttpStatus.BAD_REQUEST.toString(),
							"Cannot deposit amount. Maximum amount allowed to be deposited per day ($150k) exceeded");
				}
			}
		}
	}
}
