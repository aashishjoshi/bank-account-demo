package com.bank.account.validators;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.bank.account.models.WithdrawalRequest;
import com.bank.account.service.AccountService;

@Component
public class WithdrawalValidator implements Validator {

	private final AccountService accountService;

	@Autowired
	public WithdrawalValidator(AccountService accountService) {
		this.accountService = accountService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return WithdrawalRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		WithdrawalRequest withdrawal = (WithdrawalRequest) obj;
		// Check for max transactions in the day
		List<Double> withdrawalLog = accountService.getTodaysWithdrawalLog();

		if (withdrawal.getAmount() > accountService.getBalance()) {
			errors.reject(HttpStatus.BAD_REQUEST.toString(), "Insufficient account balance");
		} else if (withdrawalLog != null) {
			if (withdrawalLog.size() >= 3) {
				// add error for max number of transaction per day exceeded
				errors.reject(HttpStatus.BAD_REQUEST.toString(),
						"Cannot withdraw amount. Maximum of only 3 withdrawls are allowed in a day");
			} else {
				Double totalWithdrawal = withdrawalLog.stream().mapToDouble(d -> d.doubleValue()).sum();
				// add error for max withdrawl amount for the day exceeded
				if (totalWithdrawal + withdrawal.getAmount() > 50000) {
					errors.reject(HttpStatus.BAD_REQUEST.toString(),
							"Cannot withdraw amount. Maximum amount allowed to be withdrawn per day ($50k) exceeded");
				}
			}
		}
	}
}
